var express = require('express');
var mongoose = require('mongoose');
var userRouter = express.Router();
var amogueModel = mongoose.model('amogue');
var responseGenerator = require('../../libs/responseGenerator');

module.exports.controller = function (app){


userRouter.get('/',function(req,res){
        res.render('main');

    });//end main screen

userRouter.get('/admin', function (req, res) {

  res.send("This is an Admin Panel(created by Varsha Varshney) .");

});

//route 
userRouter.post('/look/create', function (req, res) {
    var newLook = new amogueModel({
            lookTypeList	 : req.body.lookTypeList,
            heightsList  	 : req.body.heightsList,
            colorsList   	 : req.body.colorsList ,
            menShapesList 	 : req.body.menShapesList,
            });
    
    
    newLook.save(function(error,result){
        if(error){
            console.log(error);
            //res.send(error);
            next(error);
        }
        else{
            res.send(result);
        }
    });

});

    userRouter.get('/dashboard', function (req, res) {

        res.render('lookList', {
        });


    }); //end get dashboard


    app.use(function (err, req, res, next) {
    	console.log(err.status);
        res.status(err.status || 500);
        if (err.status == 404) {
            res.render('404', {

                message: err.message,
                error: err
            });
        } else {
            res.render('error', {
                message: err.message,
                error: err
            });
        }
    });

    // this should be the last line
    // now making it global to app using a middleware
    // think of this as naming your api 
    app.use('/amogue', userRouter);

} //end contoller code